package main

import (
	"fmt"

	"gitlab.com/catastrophic/assistance/logthis"

	"gitlab.com/passelecasque/obstruction/tracker"
)

const (
	DefaultConfigurationFile = "config.yaml"

	fullName = "oddcheck"
)

var (
	Version = "dev"
)

func main() {
	var err error
	config, err = NewConfig(DefaultConfigurationFile)
	if err != nil {
		fmt.Println(err.Error())
		return
	}

	// log in & search for duplicates
	b, err := tracker.NewBibliotik("https://bibliotik.me", "", "", config.Bibliotik.Cookie, fullName+"/"+Version)
	if err != nil {
		fmt.Println(err.Error())
		return
	}
	/*
		// setting at 1 search/s
		if err := b.SetRateLimiter(1, 1000); err != nil {
			fmt.Println(err.Error())
			return
		}
		b.StartRateLimiter()
	*/

	if err := b.Login(); err != nil {
		fmt.Println(err.Error())
		return
	}
	logthis.TimedInfo("Logging in bibliotik.", logthis.NORMAL)

	// launching the websocket server
	WebSocketServer(b)
}

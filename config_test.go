package main

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestConfig(t *testing.T) {
	fmt.Println("+ Testing Config...")
	check := assert.New(t)

	// setting up
	c := &Config{}
	err := c.Load("test/config.yaml")
	check.Nil(err)

	// general
	fmt.Println("Checking general")
	check.Equal(3, c.General.LogLevel)

	// trackers
	fmt.Println("Checking trackers")
	check.Equal("cookievalue", c.Bibliotik.Cookie)

	fmt.Println(c.String())
}

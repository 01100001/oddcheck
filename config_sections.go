package main

import (
	"fmt"
	"strconv"

	"github.com/pkg/errors"
	"gitlab.com/catastrophic/assistance/logthis"
)

type ConfigGeneral struct {
	LogLevel int `yaml:"log_level"`
}

func (cg *ConfigGeneral) check() error {
	return logthis.CheckLevel(cg.LogLevel)
}

// String representation for ConfigGeneral.
func (cg *ConfigGeneral) String() string {
	txt := "General configuration:\n"
	txt += "\tLog level: " + strconv.Itoa(cg.LogLevel) + "\n"
	return txt
}

type ConfigBib struct {
	User     string
	Password string
	Cookie   string
}

func (cb *ConfigBib) String() string {
	txt := "Tracker configuration:\n"
	txt += "\tUser: " + cb.User + "\n"
	txt += "\tPassword: " + cb.Password + "\n"
	txt += "\tCookie: " + cb.Cookie + "\n"
	return txt
}

func (cb *ConfigBib) check() error {
	if cb.Cookie == "" && (cb.User == "" || cb.Password == "") {
		return errors.New("tracker configuration is missing information")
	}
	if cb.Cookie != "" && (cb.User != "" || cb.Password != "") {
		fmt.Println("tracker cookie information will be used to log in.")
	}
	return nil
}

package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strings"
	"sync"

	"github.com/gorilla/mux"
	"github.com/gorilla/websocket"
	"github.com/pkg/errors"
	"gitlab.com/catastrophic/assistance/logthis"
	"gitlab.com/catastrophic/assistance/ui"
	"gitlab.com/passelecasque/obstruction/tracker"
)

const (
	WebsocketPort    = 8180
	HandshakeCommand = "hello"
	SearchCommand    = "search"
	TagCommand       = "tag"

	errorIncomingWebSocketJSON = "Error parsing websocket input"
	errorOutgoingWebSocketJSON = "Error writing to websocket"
)

const (
	responseInfo = iota
	responseError
)

// IncomingJSON from the websocket created by the GM script.
type IncomingJSON struct {
	Command string
	Args    string
	Site    string
}

// OutgoingJSON to the websocket created by the GM script.
type OutgoingJSON struct {
	Status  int
	Command string
	ID      string
	Info    string
}

var (
	ErrorCreatingWebSocket = errors.New("error creating websocket")
)

func WebSocketServer(b *tracker.Bibliotik) {
	rtr := mux.NewRouter()
	var mutex = &sync.Mutex{}

	upgrader := websocket.Upgrader{
		// allows connection to websocket from anywhere
		CheckOrigin: func(r *http.Request) bool { return true },
	}
	socket := func(w http.ResponseWriter, r *http.Request) {
		c, err := upgrader.Upgrade(w, r, nil)
		if err != nil {
			logthis.Error(fmt.Errorf("%q: %w", ErrorCreatingWebSocket, err), logthis.NORMAL)
			return
		}
		defer c.Close()
		// channel to know when the connection with a specific instance is over
		endThisConnection := make(chan struct{})

		for {
			// TODO if server is shutting down, c.Close()
			incoming := IncomingJSON{}
			if err := c.ReadJSON(&incoming); err != nil {
				if !websocket.IsCloseError(err, websocket.CloseGoingAway, websocket.CloseAbnormalClosure) {
					logthis.Error(errors.Wrap(err, errorIncomingWebSocketJSON), logthis.VERBOSEST)
				}
				endThisConnection <- struct{}{}
				break
			}

			var answer OutgoingJSON
			// dealing with command
			switch incoming.Command {
			case HandshakeCommand:
				// say hello right back
				answer = OutgoingJSON{Status: responseInfo, Command: HandshakeCommand}
				// writing answer
				mutex.Lock()
				if err := c.WriteJSON(answer); err != nil {
					if !websocket.IsCloseError(err, websocket.CloseGoingAway, websocket.CloseAbnormalClosure) {
						logthis.Error(errors.Wrap(err, errorOutgoingWebSocketJSON), logthis.VERBOSEST)
					}
					endThisConnection <- struct{}{}
					break
				}
				mutex.Unlock()

			case SearchCommand:
				jsonString := strings.Replace(incoming.Args[:len(incoming.Args)-1], "window.OverDrive.titleCollection = ", "", 1)
				var books ODBooks

				if err := json.Unmarshal([]byte(jsonString), &books); err != nil {
					logthis.TimedError(err, logthis.NORMAL)
					return
				}

				for _, od := range books {
					// TODO disregard current holds

					var authors []string
					for _, author := range od.Creators {
						if author.Role == "Author" {
							authors = append(authors, author.Name)
						}
					}
					var subjects []string
					for _, sub := range od.Subjects {
						subjects = append(subjects, sub.Name)
					}
					odSubjects := "[" + strings.Join(subjects, ", ") + "]"

					publisher := od.Publisher.Name
					if len(authors) == 1 && strings.EqualFold(authors[0], publisher) {
						publisher = "Self-published"
					}
					odPublisher := " (" + publisher + ")"

					var info string
					if !od.IsAvailable {
						info = fmt.Sprintf("[%d/%d] %s", od.HoldsCount, od.OwnedCopies, publisher)
					} else {
						info = publisher
					}

					// TODO check format epub! "ebook-overdrive"? not PDF or OverDrive Read
					url := fmt.Sprintf("https://%s/media/%s", incoming.Site, od.ID)

					hits, err := b.SearchBooks(strings.Join(authors, ","), od.Title)
					if err != nil {
						logthis.TimedError(err, logthis.NORMAL)
						return
					}
					var isInteresting bool
					if len(hits) != 0 {
						// showing NON-RETAIL
						for _, h := range hits {
							if !h.IsRetail {
								isInteresting = true
								logthis.TimedInfo(ui.Yellow(fmt.Sprintf("NR | %s | %s - %s ", url, strings.Join(authors, ","), od.Title))+odSubjects+ui.BlueBold(odPublisher), logthis.NORMAL)
							}
						}
					} else {
						isInteresting = true
						var preRelease string
						if od.IsPreReleaseTitle {
							preRelease = "(pre-release) "
						}
						logthis.TimedInfo(ui.GreenBold(fmt.Sprintf("NF | %s | %s%s - %s ", url, preRelease, strings.Join(authors, ","), od.Title))+odSubjects+ui.BlueBold(odPublisher), logthis.NORMAL)
					}

					if isInteresting {
						answer = OutgoingJSON{Status: responseInfo, Command: TagCommand, ID: od.ID, Info: info}
						// writing answer
						mutex.Lock()
						if err := c.WriteJSON(answer); err != nil {
							if !websocket.IsCloseError(err, websocket.CloseGoingAway, websocket.CloseAbnormalClosure) {
								logthis.Error(errors.Wrap(err, errorOutgoingWebSocketJSON), logthis.VERBOSEST)
							}
							endThisConnection <- struct{}{}
							break
						}
						mutex.Unlock()
					}
				}
			}
		}
	}

	rtr.HandleFunc("/ws", socket)

	// serve
	logthis.TimedInfo("Launching up websocket server.", logthis.NORMAL)
	httpServer := &http.Server{Addr: fmt.Sprintf(":%d", WebsocketPort), Handler: rtr}
	if err := httpServer.ListenAndServe(); err != nil {
		if err == http.ErrServerClosed {
			logthis.Info("Closing websocker server...", logthis.NORMAL)
		} else {
			logthis.Error(errors.Wrap(err, "error with websocket server"), logthis.NORMAL)
		}
	}
}
